#!/usr/bin/env python
# coding: utf-8
"""
Ce code fonctionne
"""


class SimpleCalculator:
   # @staticmethod
    def __init__(self, var_nom_de_fonction, var_a, var_b):
        self.var_nom = var_nom_de_fonction
        self.var_x = var_a
        self.var_y = var_b

    def calcul(self):
        if isinstance(self.var_nom,str) and isinstance(self.var_x,int) and isinstance(self.var_y,int):
            if self.var_nom == "Somme":
            	return self.var_x + self.var_y
            elif self.var_nom == "Difference":
                return self.var_x - self.var_y
            elif self.var_nom == "Produit":
                 return self.var_x * self.var_y
            elif self.var_y != 0:
                 return self.var_x / self.var_y
            else:
                 raise ZeroDivisionError("Cannot divide by zero")
        else:
            raise TypeError("wrong type")

"""
###############################################
if __name__ == "__main__":
	VAL_A = int(input("Veuillez rentrer a entier... "))
	VAL_B = int(input("veuillez rentrer b entier... "))

	SimpleCalculator("Somme", VAL_A, VAL_B)
	print("Résultat de la somme : a + b = ", SimpleCalculator("Somme", VAL_A, VAL_B).calcul())

	SimpleCalculator("Difference", VAL_A, VAL_B)
	print("Résultat de la somme : a - b = ", SimpleCalculator("Difference", VAL_A, VAL_B).calcul())

	SimpleCalculator("Produit", VAL_A, VAL_B)
	print("Résultat de la somme : a * b = ", SimpleCalculator("Produit", VAL_A, VAL_B).calcul())

	SimpleCalculator("Division", VAL_A, VAL_B)
	print("Résultat de la somme : a / b = ", SimpleCalculator("Division", VAL_A, VAL_B).calcul())
"""
